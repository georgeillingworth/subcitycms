from django.db import models

from wagtail.core.models import Page
from wagtail.admin.edit_handlers import FieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.api import APIField


class ShowPage(Page):

    parent_page_type = [
        'shows.ShowsPage'  # appname.ModelName
    ]

    show_description = models.TextField(blank=True)

    banner_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=False,
        on_delete=models.SET_NULL,
        related_name = '+'
    )

    content_panels = Page.content_panels + [
        ImageChooserPanel("banner_image"),
        FieldPanel("show_description"),
    ]

    api_fields = [
        APIField('title'),
        APIField('banner_image'),
        APIField('show_description'),
    ]