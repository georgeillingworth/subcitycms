from django.db import models

from wagtail.core.models import Page
from wagtail.admin.edit_handlers import FieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel


class ShowsPage(Page):

    parent_page_type = [
        'home.HomePage'  # appname.ModelName
    ]

    subpage_types = [
        'show.ShowPage',  # appname.ModelName
    ]

    banner_title = models.CharField(max_length=100, default='Welcome to the shows page')

    introduction = models.TextField(blank=True)

    banner_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=False,
        on_delete=models.SET_NULL,
        related_name = '+'
    )

    content_panels = Page.content_panels + [
        FieldPanel("banner_title"),
        FieldPanel("introduction"),
        ImageChooserPanel("banner_image"),
    ]