from django.db import models

from wagtail.core.models import Page
from wagtail.admin.edit_handlers import FieldPanel


class HomePage(Page):

    subpage_types = [
        'shows.ShowsPage',  # appname.ModelName
        'joyous.CalendarPage',  # appname.ModelName
    ]

    banner_title = models.CharField(max_length=100, default='Welcome to my homepage')

    content_panels = Page.content_panels + [
        FieldPanel("banner_title")
    ]
